import logging
from enum import Enum
from pca9685_driver import Device

class Motor():
    def __init__(self, i2c_driver, fwd_chan, rev_chan):
        self._driver = i2c_driver
        self._fwd_chan = fwd_chan
        self._rev_chan = rev_chan
    
    def set_speed(self, speed):
        # speed -1.0 to 1.0
        if isinstance(speed, int):
            speed = float(speed)
        speed = min(1.0, max(-1.0, speed))
        
        MAX_REV=4095
        spd = int(speed * MAX_REV)
        
        logging.debug(f'{speed=}, {spd=}')
        
        if spd > 0:
            self._driver.set_pwm(self._fwd_chan, spd)
            self._driver.set_pwm(self._rev_chan, 0)
        elif spd < 0:
            self._driver.set_pwm(self._fwd_chan, 0)
            self._driver.set_pwm(self._rev_chan, -spd)
        else:
            self._driver.set_pwm(self._fwd_chan, 0)
            self._driver.set_pwm(self._rev_chan, 0)
    def __del__(self):
        self.set_speed(0)

class Motors():
    def __init__(self, i2c_bus=7, addr=0x70, pwm_freq=1000, log_level='debug'):
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s' ,level=log_level.upper())
        self.__driver = Device(bus_number=i2c_bus, address=addr)
        self.__driver.set_pwm_frequency(pwm_freq)
        self._r_motor = Motor(self.__driver, 8, 9)
        self._l_motor = Motor(self.__driver, 11, 10)
    
    def set_speed(self, l_speed, r_speed):
        self._l_motor.set_speed(l_speed)
        self._r_motor.set_speed(r_speed)
        
def main():
    from time import sleep
    
    motors = Motors(i2c_bus=7)
    
    sleep_time = 2
    val = 1
    
    motors.set_speed(val, val)
    sleep(sleep_time)
    motors.set_speed(-val, -val)
    sleep(sleep_time)
    motors.set_speed(-val, val)
    sleep(sleep_time)
    motors.set_speed(val, -val)
    sleep(sleep_time)
    
    
if __name__ == '__main__' :
    main()    
    