import logging
from smbus2 import SMBus

class RGB_LED(object):        
    def __init__(self, i2c_bus=7, addr=0x1b, debug_level='debug'):
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s' ,level=debug_level.upper())
        self._bus = SMBus(i2c_bus)
        self._addr = addr
        
    def write8(self, offset, value):
        self._bus.write_byte_data(self._addr, offset, value)
    
    def Set_All_RGB(self, R_Value, G_Value, B_Value):
        try:
            logging.debug(f'{R_Value=}, {G_Value=}, {B_Value=}')
            self.write8(0x00,0xFF)
            self.write8(0x01,R_Value)
            self.write8(0x02,G_Value)
            self.write8(0x03,B_Value)
        except:
            print ('Set_All_RGB I2C error')
    
    def OFF_ALL_RGB(self):
        try:
            self.Set_All_RGB(0x00,0x00,0x00)
        except:
            print ('OFF_ALL_RGB I2C error')
    
    def Set_An_RGB(self, Position, R_Value, G_Value, B_Value):
        try:
            if(Position <= 0x09):
                self.write8(0x00,Position)
                self.write8(0x01,R_Value)
                self.write8(0x02,G_Value)
                self.write8(0x03,B_Value)
        except:
            print ('Set_An_RGB I2C error')
    def Set_WaterfallLight_RGB(self):
        try:
            # self.OFF_ALL_RGB()
            self.write8(0x04, 0x00)
        except:
            print ('Set_WaterfallLight_RGB I2C error')
    def Set_BreathColor_RGB(self):
        try:
            # self.OFF_ALL_RGB()
            self.write8(0x04, 0x01)
        except:
            print ('Set_BreathColor_RGB I2C error')
    def Set_ChameleonLight_RGB(self):
        try:
            # self.OFF_ALL_RGB()
            self.write8(0x04, 0x02)
        except:
            print ('Set_ChameleonLight_RGB I2C error')
    #确保颜色值在0-6中
    def Set_BreathSColor_RGB(self, color):
        try:
            self.write8(0x05, color)
        except:
            print ('Set_BreathSColor_RGB I2C error')
    #确保速度设置值在1,2,3中
    def Set_BreathSSpeed_RGB(self, speed):
        try:
            self.write8(0x06, speed)
        except:
            print ('Set_BreathSSpeed_RGB I2C error')
    def Set_BreathSLight_RGB(self):
        try:
            # self.OFF_ALL_RGB()
            self.write8(0x04, 0x03)
        except:
            print ('Set_BreathSLight_RGB I2C error')
            
            
if __name__ == "__main__":
    from time import sleep
    leds = RGB_LED(i2c_bus=7, addr=0x1b)
    
    for i in range(3):
        colors = [0]*3
        colors[i] = 0xff
        logging.debug(f'{colors=}')
        
        leds.Set_All_RGB(*colors)
        sleep(1)

    leds.OFF_ALL_RGB()
    sleep(1)
    leds.Set_All_RGB(0xff, 0xff, 0xff)
    sleep(1)