
import logging
from smbus2 import SMBus, i2c_msg

class Power():
    def __init__(self, i2c_bus=7, addr=0x1b, debug_level='info'):
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s' ,level=debug_level.upper())
        self._bus = SMBus(i2c_bus)
        self._addr = addr
        
    def read(self, reg):
        ret = self._bus.read_i2c_block_data(self._addr, reg, 2)
        val = int.from_bytes(ret, byteorder='big')
        return val

    def write(self, reg, val):
        self._bus.write_byte_data(self._addr, reg, val)
        self._logger.debug("Wrote 0x%02X to register 0x%02X",
                     value, register)
    
    def get_power(self):
        val = self.read(0x00)
        volt = val * 13.3 / 1023.0
        logging.debug(f'{volt=:.3f}V')
        return volt
        
        
if __name__ == '__main__':
    power = Power(addr=0x1b, debug_level='debug')
    logging.info(f'volt = {power.get_power()}')
    