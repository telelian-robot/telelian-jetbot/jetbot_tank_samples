import logging
import serial

def checksum(buf):
    # chksum 방식이 이상함.
    sum = 0 
    for c in buf:
        sum += int(c)
    sum = ~sum 
    return sum & 0xff

class ServoSerial:
    '''
    # index 
    - 1: servo A, rotational servo
    - 2: servo B, camera updown servo
    '''
    def __init__(self, port='/dev/ttyTHS0', baudrate=115200, timeout=0.001, log_level='info'):
        logging.basicConfig(format='%(asctime)s %(levelname)-1s [%(filename)s:%(lineno)d] %(message)s' ,level=log_level.upper())
        self.__serial = serial.Serial(port, baudrate, timeout=timeout)
        self.__serial.flush()
        logging.info(f'{self.__class__.__name__} init')
    
    def __del__(self):
        self.__serial.close()
        logging.info(f'{self.__class__.__name__} closed')
        
    def set_rotation_angle_degree(self, angle, decompose_time=10):
        # center = 2330
        angle_lsb = 2330 - int(angle * 15.778) 
        self.set_angle(1, angle_lsb, decompose_time)
    
    def set_updown_angle_degree(self, angle, decompose_time=10):
        # lower limit -4 degree = 1300
        # zero = 1360
        angle_lsb = 1360 + int(angle * 15.778)
        self.set_angle(2, angle_lsb, decompose_time)
        
    def set_angle(self, index, angle, decompose_time=10):
        angle = self._get_limited_angle(index, angle)
        buf = [
            0xff,   # pack
            0xff,   # pack
            index, 
            0x07,   # length 
            0x03,   # cmd
            0x2a,   # addr
            (angle>>8)&0xff,
            angle&0xff,
            (decompose_time >> 8)&0xff,   # time_H 
            decompose_time&0xff           # time_L
        ]
        buf.append(checksum(buf[2:]))
        logging.debug(f'{buf=}')
        self.__serial.write(bytes(buf))
        
    def set_angle_multi(self, angle_horizontal, angle_vertical, decompose_time=10):
        angle_horizontal = self._get_limited_angle(1, angle_horizontal)
        angle_vertical = self._get_limited_angle(2, angle_vertical)
        buf = [
            0xff,   # pack
            0xff,   # pack
            0xfe,   # multi servo id
            0x0e,   # length 
            0x83,   # cmd
            0x2a,   # addr
            0x04,   # addr
            1,
            (angle_horizontal>>8)&0xff,
            angle_horizontal&0xff,
            (decompose_time >> 8)&0xff,   # time_H 
            decompose_time&0xff,           # time_L
            2,
            (angle_vertical>>8)&0xff,
            angle_vertical&0xff,
            (decompose_time >> 8)&0xff,   # time_H 
            decompose_time&0xff,           # time_L
        ]
        buf.append(checksum(buf[2:]))
        logging.debug(f'{buf=}')
        self.__serial.write(bytes(buf))
        
    def _get_limited_angle(self, index, angle):
        if index == 1:
            angle = max(600, min(4000, angle))
        else:
            angle = max(1300, min(4095, angle))
        return angle

if __name__ == '__main__':
    from time import sleep
    servo = ServoSerial()
            
    for i in range(3):
        # servo.set_rotation_angle_degree(-90)
        # sleep(1)
        # servo.set_rotation_angle_degree(90)
        # sleep(1)
        # servo.set_rotation_angle_degree(0)
        # sleep(1)
        
        servo.set_updown_angle_degree(90)
        sleep(1)
        servo.set_updown_angle_degree(0)
        sleep(1)
