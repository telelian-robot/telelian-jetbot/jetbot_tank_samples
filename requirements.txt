cffi==1.15.1
PCA9685-driver==1.2.0
pycparser==2.21
pyserial==3.5
smbus-cffi==0.5.1
smbus2==0.4.2